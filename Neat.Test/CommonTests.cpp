#include <boost/test/unit_test.hpp>

#include "NeatCore/numbers.h"

#define BOOST_CHECK_IN_BETWEEN(x, min, max) BOOST_CHECK(x >= min && x <= max); if (!(x >= min && x <= max)) BOOST_TEST_MESSAGE( #x << " should be between " << min << " and " << max << " but was " << x);

int iteration_count = 1E6;

BOOST_AUTO_TEST_SUITE(TS_CommonTests)

BOOST_AUTO_TEST_SUITE(TS_RandomNumbers)

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_RandomNumbers_RandomNumbers_INT)
{
   using type = int;
   bool done = true;
   for (int n = 0; n < iteration_count; n++)
   {
      type num = random_number<type>(0, 100);
      done &= (num >= 0 && num <= 100);
   }

   BOOST_CHECK(done);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_RandomNumbers_RandomNumbers_FLOAT)
{
   using type = float;
   bool done = true;
   for (int n = 0; n < iteration_count; n++)
   {
      type num = random_number<type>(0, 100);
      done &= (num >= 0 && num <= 100);
   }
   BOOST_CHECK(done);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_RandomNumbers_RandomNumbers_DOUBLE)
{
   using type = double;
   bool done = true;
   for (int n = 0; n < iteration_count; n++)
   {
      type num = random_number<type>(0, 100);
      done &= (num >= 0 && num <= 100);
   }
   BOOST_CHECK(done);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_RandomNumbers_RandomNumbers_ULONGLONG)
{
   using type = unsigned long long;
   bool done = true;
   for (int n = 0; n < iteration_count; n++)
   {
      type num = random_number<type>(0, 100);
      done &= (num >= 0 && num <= 100);
   }
   BOOST_CHECK(done);
}


BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_RandomNumbers_RandomNumbersAdvanced_FLOAT)
{
   using type = float;
   bool done = true;
   for (int n = 0; n < iteration_count; n++)
   {
      type num = random_number<type>(-123.893f, 1000.124f);
      done &= (num >= -123.893f && num <= 1000.124f);
   }

   BOOST_CHECK(done);
   BOOST_TEST_MESSAGE("samples:");
   BOOST_TEST_MESSAGE(random_number<type>(-123.893f, 1000.124f));
   BOOST_TEST_MESSAGE(random_number<type>(-123.893f, 1000.124f));
   BOOST_TEST_MESSAGE(random_number<type>(-123.893f, 1000.124f));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TS_TakeRandom)

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_INT)
{
   int a = 1, b = 2;
   int rand = take_random(a, b);
   BOOST_CHECK(rand == a || rand == b);

   int c = 3;
   rand = take_random(a, b, c);
   BOOST_CHECK(rand == a || rand == b || rand == c);

   int d = 4;
   rand = take_random(a, b, c, d);
   BOOST_CHECK(rand == a || rand == b || rand == c || rand == d);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_VEC)
{
   int result = take_random<int>({ 1, 2, 3, 4 });
   BOOST_CHECK_IN_BETWEEN(result, 1, 4);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_STRING)
{
   std::string a = "a", b = "b", c = "c";
   std::string result = take_random(a, b, c);
   BOOST_CHECK(result == a || result == b || result == c);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_REFERENCES)
{
   std::string a = "a", b = "b", c = "c";
   std::string& result = take_random(a, b, c);
   BOOST_CHECK(&result == &a || &result == &b || &result == &c);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_COPY)
{
   int result = take_random(1, 2, 3);
   BOOST_CHECK(result == 1 || result == 2 || result == 3);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_REF_VECTORS)
{
   std::vector<int> a = { 1, 2 }, b = { 1, 2, 3 };
   auto& result = take_random(a, b);
   BOOST_CHECK(&result == &a || &result == &b);
}

BOOST_AUTO_TEST_CASE(TS_CommonTests_TS_TakeRandom_TakeRandom_COP_VECTORS)
{
   auto result = take_random(std::vector<int> { 1, 2 }, std::vector<int> { 1, 2, 3 });
   BOOST_CHECK(result.size() == 2 || result.size() == 3);
   BOOST_CHECK(result[0] == 1 && result[1] == 2);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
