#include <boost\test\unit_test.hpp>

#include "NeatCore/neatcore.h"
#include "NeatCore/iactivationfunction.h"
#include "NeatCore/activationfunctionfactory.h"

#include <vector>
#include <map>
#include <memory>
#include <math.h>

#include <QtCore/QString>
#include <QtCore/QStringList>


DEFINE_ACTIVATION_FUNCTION(QUADRATIC, x * x, 2 * x);

BOOST_AUTO_TEST_SUITE(TS_ActivationFunctions);

BOOST_AUTO_TEST_CASE(TS_ActivationFunctions_ActivationFunctionsAreRegistered)
{
   auto names = ActivationFunctionFactory::GetNames();
   BOOST_CHECK(names.contains("SIN"));
   BOOST_CHECK(names.contains("COS"));
   BOOST_CHECK(names.contains("SIGMOID"));
   BOOST_CHECK(names.contains("LINEAR"));
}

BOOST_AUTO_TEST_CASE(TS_ActivationFunctions_ActivationFunctionsWorks)
{
   auto sin_fn = ActivationFunctionFactory::CreateInstance("SIN");
   auto cos_fn = ActivationFunctionFactory::CreateInstance("COS");
   auto lin_fn = ActivationFunctionFactory::CreateInstance("LINEAR");
   BOOST_CHECK_EQUAL(sin(10), sin_fn->calculate(10));
   BOOST_CHECK_EQUAL(cos(10), sin_fn->calculate_derivative(10));
   BOOST_CHECK_EQUAL(cos(10), cos_fn->calculate(10));
   BOOST_CHECK_EQUAL(-sin(10), cos_fn->calculate_derivative(10));
   BOOST_CHECK_EQUAL(10, lin_fn->calculate(10));
   BOOST_CHECK_EQUAL(1, lin_fn->calculate_derivative(10));
}

BOOST_AUTO_TEST_CASE(TS_ActivationFunctions_SelfDefinedActivationFunctionIsRegistered)
{
   auto names = ActivationFunctionFactory::GetNames();
   BOOST_CHECK(names.contains("QUADRATIC"));
   auto fn = ActivationFunctionFactory::CreateInstance("QUADRATIC");
   BOOST_CHECK_EQUAL(100, fn->calculate(10));
   BOOST_CHECK_EQUAL(20, fn->calculate_derivative(10));
}

BOOST_AUTO_TEST_SUITE_END()
