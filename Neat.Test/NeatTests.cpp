#include <boost/test/unit_test.hpp>

#define USE_LOG

#include "NeatCore/neatcore.h"
#include "NeatCore/neatpreferences.h"

static NeatPreferences prefs(
   100,
   2,
   2,
   0.04,
   0.03,
   0.001,
   0.05,
   0.001,
   1);

struct NeatFixture
{
   NeatFixture()
      : neat(prefs)
   {
   }

protected:
   Neat neat;
};


bool _registered = Log::register_observer([](Log::info_pack info)
{
   BOOST_TEST_MESSAGE("[" << info.time << "] " << info.file << " (" << info.line << "): [" << info.function << "] " << info.mes << "\n");
});

BOOST_FIXTURE_TEST_SUITE(TS_Neat, NeatFixture)

BOOST_AUTO_TEST_CASE(TS_Neat_PreferencesCreationFailesWithIncorrectArgs)
{
   const auto creation = []() { NeatPreferences(0, 0, 0, 0, 0, 0, 0, 0, 0); };

   BOOST_CHECK_THROW(creation(), std::exception);
}

BOOST_AUTO_TEST_CASE(TS_Neat_NeatCreationDoesntThrowBecauseOfRights)
{
   auto creation = []() { Neat neat(prefs); };
   BOOST_CHECK_NO_THROW(creation());
}

BOOST_FIXTURE_TEST_CASE(TS_Neat_NeatIsAliveWhenInitialized, NeatFixture)
{
   BOOST_CHECK_EQUAL(true, neat.IsAlive());
}

BOOST_FIXTURE_TEST_CASE(TS_Neat_NeatCreatesFirstGeneration, NeatFixture)
{
   bool done = true;
   for (int ID = 0; ID < prefs.generation_size; ID++)
   {
      auto network = neat.GetNeuralNetworkByID(ID);
      done &= (network.get() != nullptr);
      done &= (ID == network->ID);

      if (!done)
      {
         // returns means that this boost macros will run only once!
         BOOST_TEST_MESSAGE("ID is " << network->ID << " but should be " << ID);
         if (network.get() == nullptr)
            BOOST_TEST_MESSAGE("network is null");
         return;
      }
   }
}

BOOST_FIXTURE_TEST_CASE(TS_Neat_FirstGenerationReturnsZeros, NeatFixture)
{
   Neat::NeatDataSPtr data = std::make_shared<std::vector<double>>();
   data->push_back(2.0);
   data->push_back(-2.0);
   Neat::NeatData result = neat.RunBest(data);
   BOOST_CHECK_EQUAL(2, result.size());
   BOOST_CHECK_EQUAL(0, result[0]);
   BOOST_CHECK_EQUAL(0, result[1]);
}

BOOST_FIXTURE_TEST_CASE(TS_Neat_After100IterationsThereAreConnections, NeatFixture)
{
   Neat::NeatDataSPtr data(new Neat::NeatData());
   data->push_back(2.0);
   data->push_back(-2.0);

   bool connected = false;

   for (int n = 0; n < 100; n++)
   {
      Neat::NeatData result = neat.RunBest(data);

      for (int i = 0; i < prefs.generation_size; i++)
         if (!neat.GetNeuralNetworkByID(i)->get_genomes().empty())
         {
            const auto genome = neat.GetNeuralNetworkByID(i)->get_genomes()[0];
            result = neat.GetNeuralNetworkByID(i)->run(data);

            // double break out! this messages will run once!
            BOOST_TEST_MESSAGE("after generation " << n << " a connection was found in neural network " << i);
            BOOST_TEST_MESSAGE("genome connects neuron " << genome->input_neuron->ID << " and " << genome->output_neuron->ID);
            BOOST_TEST_MESSAGE("genome has weight [" << genome->weight << "]");
            BOOST_TEST_MESSAGE("new output is { " << result[0] << " , " << result[1] << " }");
            BOOST_CHECK(genome->input_neuron->ID != genome->output_neuron->ID);
            connected = true;
            break;
         }

      if (connected) break;

      for (int i = 0; i < prefs.generation_size; i++)
      {
         auto nn = neat.GetNeuralNetworkByID(i);
         result = nn->run(data);
         const double offset = abs((*data)[0] - result[0]) + abs((*data)[1] - result[1]);
         neat.AssignScore(i, offset);
      }

      neat.EvaluateScoresAndTrain();
   }

   BOOST_CHECK_EQUAL(true, connected);
}

BOOST_FIXTURE_TEST_CASE(NeatCanLearnXORgate, NeatFixture)
{
   int iterations = 1000;
   bool found = false;

   Neat::NeatEvaluationFunction fn = [](NeuralNetwork::SPtr network, Neat::NeatData data) -> double
   {
      auto result = network->run(std::make_shared<Neat::NeatData>(data));
      return 1.0 / abs((data[0] == data[1] ? 0 : 1) - result[0]);
   };

   Neat::NeatDataCollectionSPtr db(new Neat::NeatDataCollection
   {
      { 0, 0 },
      { 1, 0 },
      { 0, 1 },
      { 1, 1 }
   });

   while (iterations-- > 0 && !found)
   {
      neat.Train(db, fn);

      double result = 0;
      for (int n = 0; n < db->size(); n++)
         result += fn (neat.BestNetwork(), db->at(n));

      if (result <= 10E-3)
         found = true;
   }

   BOOST_CHECK(found);
   for (int n = 0; n < db->size(); n++)
      BOOST_TEST_MESSAGE("input (" << n << ")   output (" << neat.RunBest(std::make_shared<Neat::NeatData>(db->at(n)))[0] << ")  score (" << fn(neat.BestNetwork(), db->at(n)) << ")");
}

BOOST_AUTO_TEST_SUITE_END()
