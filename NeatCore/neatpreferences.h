#pragma once

#include "Export.h"

class NEATCORE_EXPORT NeatPreferences
{
public:  
   
   static const NeatPreferences Std;
   
   NeatPreferences(
      int generation_size,
      int input_size,
      int output_size,
      float neuron_probability,
      float weight_probability,
      float random_weight_probability,
      float genome_probability,
      float disjoint_probability,
      double selection_size_percentage
   );

   NeatPreferences() { }

   NeatPreferences(const NeatPreferences&) = default;

   int
      generation_size,
      input_size,
      output_size;

   float
      neuron_probability,
      weight_probability,
      random_weight_probability,
      genome_probability,
      disjoint_probability;

   double 
      selection_size_percentage;

private: 

   static const NeatPreferences GetNewStdPreferences();
};