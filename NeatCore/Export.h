#pragma once

#include <QtCore/qglobal.h>
#include <memory>

#ifndef BUILD_STATIC
# if defined(NEATCORE_LIB)
#  define NEATCORE_EXPORT Q_DECL_EXPORT
# else
#  define NEATCORE_EXPORT Q_DECL_IMPORT
# endif
#else
# define NEATCORE_EXPORT
#endif

// ##################################################
// ##################### MEMORY #####################
// ##################################################

#define MEMORY(class_name)                                                                     \
typedef std::shared_ptr<class_name> SPtr;                                                      \
template<typename ...Args>                                                                     \
static SPtr CreateSPtr(Args... args) { return std::make_shared<class_name>(args...); };        \
typedef std::unique_ptr<class_name> UPtr;                                                      \
template<typename ...Args>                                                                     \
static UPtr CreateUPtr(Args... args) { return std::make_unique<class_name>(args...); };

#include <functional>
#include <vector>
#include <ctime>

// ##################################################
// #################### Logging #####################
// ##################################################

#define USE_LOG

#if defined(USE_LOG)
#  define _STR(x) #x
#  define STR(x) _STR(X)
#  define LOG(mes) Log::log(__FUNCTION__, __FILE__, __LINE__, mes)
#else
#  define LOG(mes)
#endif


struct NEATCORE_EXPORT Log
{
   struct info_pack { const char* function; const char* file; int line; const char* mes; const char* time; };

   static void log(const char* function, const char* file, int line, const char* mes);

   static bool register_observer(std::function<void(info_pack)> function);

private:

   static std::vector<std::function<void(info_pack)>> functions;

};