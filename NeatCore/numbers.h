#pragma once

#include <cstdlib>
#include <vector>
#include <memory>
#include <functional>

void init_random();

/// @brief					generates a random number between min and max (both INCLUDED!) of type T
/// @param	min		   the min value
/// @param	max		   the max value				
/// @returns				the randomly generated number
template<typename T>
T random_number(T min, T max)
{
   return T((rand() / double(RAND_MAX)) * (max - min) + min);
}

/// @brief					default concat case if no arguments
/// @returns				an empty list
template<typename T>
std::vector<T*> concat()
{
   return std::vector<T*>();
}

/// @brief					default concat case if one argument
/// @param	first		   any generic argument
/// @returns				a list with just param $first in it
template<typename T>
std::vector<T*> concat(T& first)
{
   return std::vector<T*> { &first };
}

/// @brief					default concat case if two arguments
/// @param	first		   any generic argument
/// @param	second		any generic argument
/// @returns				a list with $first and $second in it
template<typename T>
std::vector<T*> concat(T& first, T& second)
{
   std::vector<T*> list;
   list.push_back(&first);
   list.push_back(&second);
   return list;
}

/// @brief					concatenates every argument to a std::vector
/// @note					enabled if argument size is at least 3
/// @param	first		   any generic argument
/// @param	rest  		any number of generic arguments
/// @returns				all arguments in a std::vector
template<typename T, typename ... _T, typename = typename std::enable_if<sizeof...(_T) >= 2>::type>
std::vector<T*> concat(T& first, _T& ... rest)
{
   std::vector<T*> list;
   list.push_back(&first);

   for (auto item : concat(rest...))
      list.push_back(item);

   return list;
}

/// @brief					default case returning default constructed generic type
/// @returns				default constructed generic type
template<typename T>
T& take_random()
{
   return T {};
}

/// @brief					default case for one argument
/// @param	first		   any generic argument				
/// @returns				the $first argument
template
<
   typename T,
   typename = typename std::enable_if<std::is_class<T>::value == std::true_type::value>::type
>
T& take_random(T& first)
{
   return first;
}


/// @brief					default case for one argument
/// @param	first		   any generic argument				
/// @returns				the $first argument
template
<
   typename T,
   typename = typename std::enable_if<std::is_class<T>::value == std::false_type::value>::type
>
T take_random(T first)
{
   return first;
}

/// @brief					default case for two arguments
/// @param	first		   any generic argument
/// @param	second		any generic argument
/// @returns				randomly either $first or $second
template
<
   typename T,
   typename = typename std::enable_if<std::is_class<T>::value == std::true_type::value>::type
>
T& take_random(T& first, T& second)
{
   std::vector<T*> list;
   list.push_back(&first);
   list.push_back(&second);

   int random_index = random_number<size_t>(0, list.size() - 1);
   return *list[random_index];
}

/// @brief					default case for two arguments
/// @param	first		   any generic argument
/// @param	second		any generic argument
/// @returns				randomly either $first or $second
template
<
   typename T,
   typename = typename std::enable_if<std::is_class<T>::value == std::false_type::value>::type
>
T take_random(T first, T second)
{
   std::vector<T*> list;
   list.push_back(&first);
   list.push_back(&second);

   int random_index = random_number<size_t>(0, list.size() - 1);
   return *list[random_index];
}

/// @brief					take a randomly chosen argument
/// @note					enabled if argument size is at least 3
/// @param	first		   any generic argument
/// @param	rest		   any number of generic arguments
/// @returns				a randomly chosen argument
template
<
   typename T, 
   typename ... _T, 
   typename = typename std::enable_if<sizeof...(_T) >= 2>::type,
   typename = typename std::enable_if<std::is_class<T>::value == std::true_type::value>::type
>
T& take_random(T& first, _T& ... rest)
{
   std::vector<T*> list = concat(first, rest...);

   int random_index = random_number<size_t>(0, list.size() - 1);
   return *list[random_index];
}

/// @brief					take a randomly chosen argument
/// @note					enabled if argument size is at least 3
/// @param	first		   any generic argument
/// @param	rest		   any number of generic arguments
/// @returns				a randomly chosen argument
template
<
   typename T,
   typename ... _T,
   typename = typename std::enable_if<sizeof...(_T) >= 2>::type,
   typename = typename std::enable_if<std::is_class<T>::value == std::false_type::value>::type,
   typename = typename std::enable_if<std::is_copy_constructible<T>::value>::type
>
T take_random(T first, _T ... rest)
{
   std::vector<T*> list = concat(first, rest...);

   int random_index = random_number<size_t>(0, list.size() - 1);
   return *list[random_index];
}

/// @brief					take a random object inside the $list
/// @param	list		   the input list of any generic type
/// @returns				a randomly chosen object of that $list
template<typename T>
T take_random(std::vector<T> list)
{
   int random_index = random_number<size_t>(0, list.size() - 1);
   return list[random_index];
}

template<typename T>
std::vector<T> combine_expr(std::vector<T>& a, std::vector<T>& b, std::function<bool(T&, T&)> compare_fn)
{
   std::vector<T> list;
   for (auto& _a : a)
      for (auto& _b : b)
         if (compare_fn(_a, _b))
            list.push_back(_a);
   return list;
}

template<typename T>
std::vector<T> combine(std::vector<T>& a, std::vector<T>& b)
{
   std::vector<T> list;
   for (auto& _a : a)
      for (auto& _b : b)
      {
         list.push_back(_a);
         list.push_back(_b);
      }
   return list;
}