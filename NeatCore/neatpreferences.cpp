#include "neatpreferences.h"

const NeatPreferences NeatPreferences::Std = NeatPreferences::GetNewStdPreferences();

NeatPreferences::NeatPreferences(int generation_size,
                                 int input_size,
                                 int output_size,
                                 float neuron_probability,
                                 float weight_probability,
                                 float random_weight_probability, 
                                 float genome_probability, 
                                 float disjoint_probability,
                                 double selection_size_percentage)
   :  generation_size(generation_size),
      input_size(input_size),
      output_size(output_size),
      neuron_probability(neuron_probability),
      weight_probability(weight_probability),
      random_weight_probability(random_weight_probability),
      genome_probability(genome_probability),
      disjoint_probability(disjoint_probability),
      selection_size_percentage(selection_size_percentage)
{
   if(selection_size_percentage * generation_size < 2)
      throw std::exception("The selection size (%) has to result in at least two networks (selection_size_percentage * generation_size >= 2)!");

   if (input_size <= 0 || output_size <= 0)
      throw std::exception("input and output size have to be greater than 0!");

   if (selection_size_percentage * generation_size > generation_size)
      throw std::exception("The selection size has to be less than the generation size (but greater or equal to 2)");
}

const NeatPreferences NeatPreferences::GetNewStdPreferences()
{
   NeatPreferences pref;

   return pref;
}
