#pragma once

#include "Export.h"
#include "neatpreferences.h"
#include "neuralnetworkpool.h"

class NEATCORE_EXPORT CrossoverManager
{
public:

   CrossoverManager() = delete;
   CrossoverManager(const CrossoverManager&) = delete;

   CrossoverManager(const NeatPreferences& prefs, NeuralNetworkPool& pool);

   ~CrossoverManager() = default;

   void recreate_population(std::vector<NeuralNetwork::SPtr> best);

private:

   const NeatPreferences& prefs;

   NeuralNetworkPool& pool;

};
