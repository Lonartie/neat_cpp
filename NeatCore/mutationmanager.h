#pragma once

#include "Export.h"
#include "neatpreferences.h"
#include "neuralnetworkpool.h"
#include "genome.h"

class Neat;

class NEATCORE_EXPORT MutationManager
{
public:

   MutationManager() = delete;

   MutationManager(const NeatPreferences& prefs, NeuralNetworkPool& pool);

   void mutate_all();

   void mutate_network(NeuralNetwork::SPtr neural_network);
   
private:

   void disjoint_random_genome(NeuralNetwork::SPtr neural_network);

   void connect_2_random_neurons(NeuralNetwork::SPtr neural_network);

   void re_init_random_weight(NeuralNetwork::SPtr neural_network);

   void change_weight_randomly(Genome::SPtr genome);

   void add_random_neuron(NeuralNetwork::SPtr neural_network);

   const NeatPreferences& prefs;

   NeuralNetworkPool& pool;
};
