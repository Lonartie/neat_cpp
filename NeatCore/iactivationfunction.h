#pragma once

#include "Export.h"
#include "activationfunctionfactory.h"
#include <memory>
#include <QtCore/QString>

class NEATCORE_EXPORT IActivationFunction
{
public:

   typedef std::shared_ptr<IActivationFunction> SPtr;

   virtual double calculate(double x) = 0;

   virtual double calculate_derivative(double x) = 0;

   virtual QString get_name() = 0;
};

#define DEFINE_ACTIVATION_FUNCTION(name, fn, drv)                                                                       \
class name : public IActivationFunction                                                                                 \
{                                                                                                                       \
public:                                                                                                                 \
   name##() = default;                                                                                                  \
   ~##name##() = default;                                                                                               \
   virtual double calculate(double x) override { return fn##; }                                                         \
   virtual double calculate_derivative(double x) override { return drv##; };                                            \
   virtual QString get_name() override { return #name##; };                                                             \
   static std::shared_ptr<IActivationFunction> CreateInstance();                                                        \
};                                                                                                                      \
std::shared_ptr<IActivationFunction> name##::CreateInstance() { return std::make_shared<##name##>(); };                 \
bool name##_registered = ActivationFunctionFactory::RegisterActivationFunction(##name##::CreateInstance, #name##);                                              
