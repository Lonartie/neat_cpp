#include "Export.h"
#include <chrono>

std::vector<std::function<void(Log::info_pack)>> Log::functions;

void Log::log(const char* function, const char* file, int line, const char* mes)
{
   auto now = std::chrono::system_clock::now();
   auto time_t = std::chrono::system_clock::to_time_t(now);
   const char* time = ctime(&time_t);

   for (auto& fn : functions)
      fn(info_pack{ function, file, line, mes, time });
}

bool Log::register_observer(std::function<void(info_pack)> function)
{
   functions.push_back(function);
   return true;
}
