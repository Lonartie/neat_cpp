#pragma once

#include "Export.h"
#include "neuralnetwork.h"
#include "neuron.h"
#include "genome.h"
#include "neatpreferences.h"

#include <memory>
#include <vector>

class Neat;

class NEATCORE_EXPORT NeuralNetworkPool
{
public:

   explicit NeuralNetworkPool(const NeatPreferences& prefs, const Neat& neat);

   std::shared_ptr<NeuralNetwork> create_neural_network();

   std::shared_ptr<NeuralNetwork> reset_neural_network(NeuralNetwork::SPtr network);

   std::vector<std::shared_ptr<NeuralNetwork>> get_networks() const;

   void register_creation_observer(std::function<void(NeuralNetwork::SPtr)> obs_function);

   Neuron::SPtr get_random_neuron(const NeuralNetwork::SPtr network) const;

   Neuron::SPtr get_random_neuron(const NeuralNetwork::SPtr network, int ID) const;

   Genome::SPtr connect_neurons(NeuralNetwork::SPtr network, int ID_a, int ID_b) const;

   void connect_neurons(Neuron::SPtr a, Neuron::SPtr b, Genome::SPtr g) const;

   Neuron::SPtr create_neuron(NeuralNetwork::SPtr network) const;

   NeuralNetwork::SPtr overwrite_neural_network(int i);

private:

   void notify_observers(NeuralNetwork::SPtr new_nn);

   const NeatPreferences& prefs;

   static void check_rights(const Neat& neat);

   std::vector<std::function<void(NeuralNetwork::SPtr)>> observers;

   std::vector<std::shared_ptr<NeuralNetwork>> _networks;

   const Neat& neat;
};