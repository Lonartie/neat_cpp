#pragma once

#include "Export.h"
#include "neuralnetwork.h"
#include "neuralnetworkpool.h"
#include "neatpreferences.h"
#include "enums.h"

#include <vector>
#include <memory>

struct NEATCORE_EXPORT NeatCoreStorage
{
public:

   NeatCoreStorage(const NeatPreferences preferences, const Neat& neat) 
      :  preferences(preferences), 
         pool(preferences, neat) 
   { }

   NeuralNetworkPool pool;

   std::vector<double> scores;
   
   NeatDeviceType deviceType;

   const NeatPreferences preferences;
};