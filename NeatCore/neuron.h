#pragma once

#include "Export.h"

#include "iactivationfunction.h"

#include <memory>
#include <vector>

class Genome;

class NEATCORE_EXPORT Neuron
{
public:

   MEMORY(Neuron)

   Neuron(std::shared_ptr<IActivationFunction> ac_func, int ID, double layer);
   Neuron(const Neuron& neuron);
   ~Neuron();

   double get_output ();
   
   std::shared_ptr<IActivationFunction> ac_func;

   std::vector<std::shared_ptr<Genome>> input_genomes;
   std::vector<std::shared_ptr<Genome>> output_genomes;

   double value = 0.0;

   int ID = 0;

   bool is_input = false;

   double layer = 0;

};
