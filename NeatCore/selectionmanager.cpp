#include "selectionmanager.h"

namespace
{
   template<typename A, typename B>
   std::pair<B, A> flip_pair(const std::pair<A, B> &p)
   {
      return std::pair<B, A>(p.second, p.first);
   }

   template<typename A, typename B>
   std::multimap<B, A> flip_map(const std::map<A, B> &src)
   {
      std::multimap<B, A> dst;
      std::transform(src.begin(), src.end(), std::inserter(dst, dst.begin()),
         flip_pair<A, B>);
      return dst;
   }
}


SelectionManager::SelectionManager(NeuralNetworkPool& pool, const NeatPreferences& prefs)
   :  pool(pool),
      prefs(prefs)
{
   pool.register_creation_observer([this](auto nn) { add_neural_network_score_definition(nn); });
}

std::vector<NeuralNetwork::SPtr> SelectionManager::get_best_population() const
{
   int count = round(prefs.selection_size_percentage * pool.get_networks().size());
   std::vector<NeuralNetwork::SPtr> selection(count);
   auto sorted = flip_map(scores);

   for (auto& pair : sorted)
      if (count-- > 0)
         selection[count] = pair.second;

   return selection;
}

NeuralNetwork::SPtr SelectionManager::get_best() const
{
   auto sorted = flip_map(scores);
   for (auto& kvp : sorted)
      return kvp.second;
   throw std::exception("no registered networks");
}

void SelectionManager::set_score(const NeuralNetwork::SPtr network, double score)
{
   scores[network] = score;
}

void SelectionManager::set_score(const int ID, double score)
{
   for(auto& kvp : scores)
      if (kvp.first->ID == ID)
      {
         kvp.second = score;
         return;
      }
}

void SelectionManager::add_neural_network_score_definition(NeuralNetwork::SPtr new_nn)
{
   scores.emplace(new_nn, 0);
}
