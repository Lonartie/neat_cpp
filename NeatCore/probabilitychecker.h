#pragma once

#include "Export.h"

class NEATCORE_EXPORT ProbabilityChecker
{
public:
   
   static bool Is(double probability);

private:

   static void init();

   static bool is_initialized;

};
