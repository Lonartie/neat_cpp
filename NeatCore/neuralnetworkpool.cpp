#include "neuralnetworkpool.h"
#include "neatcore.h"
#include "numbers.h"

#include <QtCore/QString>
#include <cassert>

NeuralNetworkPool::NeuralNetworkPool(const NeatPreferences& prefs, const Neat& neat)
   : prefs(prefs),
   neat(neat)
{
}

std::shared_ptr<NeuralNetwork> NeuralNetworkPool::create_neural_network()
{
   check_rights(neat);

   auto network = NeuralNetwork::CreateSPtr(prefs, *this);
   network->ID = int(_networks.size());
   _networks.push_back(network);
   notify_observers(network);
   return network;
}

std::shared_ptr<NeuralNetwork> NeuralNetworkPool::reset_neural_network(std::shared_ptr<NeuralNetwork> network)
{
   check_rights(neat);

   return network;
}

std::vector<std::shared_ptr<NeuralNetwork>> NeuralNetworkPool::get_networks() const
{
   return _networks;
}

void NeuralNetworkPool::register_creation_observer(std::function<void(NeuralNetwork::SPtr)> obs_function)
{
   observers.push_back(obs_function);
}

Neuron::SPtr NeuralNetworkPool::get_random_neuron(const NeuralNetwork::SPtr network) const
{
   auto& neurons = network->registered_neurons;

   const auto random = random_number<int>(0, int(neurons.size()) - 1);

   if (random >= neurons.size())
      return nullptr;

   return neurons[random];
}

Neuron::SPtr NeuralNetworkPool::get_random_neuron(const NeuralNetwork::SPtr network, int ID) const
{
   auto other = network->get_neuron_by_ID(ID);

   if (!other)
      return nullptr;

   if (other->layer == 10.0)
      return nullptr;

   auto neuron = get_random_neuron(network);

   const int maxIterations = 5;
   int currentIteration = 0;
   while (neuron->ID == ID || neuron->layer <= other->layer)
   {
      neuron = get_random_neuron(network);
      if (currentIteration++ == maxIterations)
         return nullptr;
   }

   return neuron;
}

Genome::SPtr NeuralNetworkPool::connect_neurons(NeuralNetwork::SPtr network, int ID_a, int ID_b) const
{
   Neuron::SPtr neuron_a = network->get_neuron_by_ID(ID_a);
   Neuron::SPtr neuron_b = network->get_neuron_by_ID(ID_b);

   if (!neuron_a || !neuron_b)
      return nullptr;

   if (neuron_a->ID == neuron_b->ID)
      return nullptr;

   auto genome = Genome::CreateSPtr();
   genome->input_neuron = neuron_a;
   genome->output_neuron = neuron_b;
   genome->input_neuron_ID = neuron_a->ID;
   genome->output_neuron_ID = neuron_b->ID;

   neuron_b->input_genomes.push_back(genome);
   neuron_a->output_genomes.push_back(genome);

   network->registered_genomes.push_back(genome);

   return genome;
}

void NeuralNetworkPool::connect_neurons(Neuron::SPtr a, Neuron::SPtr b, Genome::SPtr g) const
{
   a->output_genomes.push_back(g);
   b->input_genomes.push_back(g);
   g->input_neuron = a;
   g->output_neuron = b;
   g->input_neuron_ID = a->ID;
   g->output_neuron_ID = b->ID;
}

Neuron::SPtr NeuralNetworkPool::create_neuron(NeuralNetwork::SPtr network) const
{
   const auto neuron_a = get_random_neuron(network);
   const auto neuron_b = get_random_neuron(network, neuron_a->ID);

   if (!neuron_b)
      return nullptr;

   auto neuron = Neuron::CreateSPtr(ActivationFunctionFactory::GetRandom(), int(network->registered_neurons.size()), (neuron_a->layer + neuron_b->layer) / 2.0);
   network->ID_table.emplace(neuron->ID, neuron);
   connect_neurons(network, neuron_a->ID, neuron->ID);
   connect_neurons(network, neuron->ID, neuron_b->ID);
   neuron->layer = (neuron_a->layer + neuron_b->layer) / 2.0;
   network->registered_neurons.push_back(neuron);
   return neuron;
}

void NeuralNetworkPool::notify_observers(NeuralNetwork::SPtr new_nn)
{
   for (auto& obs_function : observers)
      obs_function(new_nn);
}

void NeuralNetworkPool::check_rights(const Neat& neat)
{
   assert(&neat != nullptr);
   assert(neat.IsAlive());
   QString salt = neat.getSalt();
   QString salt2 = QString::fromWCharArray(
      new wchar_t[6]{ wchar_t(2), wchar_t(19), wchar_t(22), wchar_t(200), wchar_t(34), wchar_t(82) }, 6
   );
   assert(std::equal(salt.begin(), salt.end(), salt2.begin(), salt2.end()));
}

NeuralNetwork::SPtr NeuralNetworkPool::overwrite_neural_network(int i)
{
   auto network = _networks[i];

   network->registered_genomes.clear();
   network->registered_neurons.clear();
   network->layers.clear();
   network->ID_table.clear();

   network->init_layers();

   return network;
}
