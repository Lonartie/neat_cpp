#pragma once

#include "Export.h"
#include "neatpreferences.h"
#include "neatcorestorage.h"
#include "neuralnetwork.h"
#include "enums.h"
#include "neuralnetworkpool.h"
#include "mutationmanager.h"
#include "selectionmanager.h"
#include "crossovermanager.h"

#include <vector>
#include <map>
#include <memory>
#include <functional>

#include <QtCore/QString>

class NEATCORE_EXPORT Neat
{
public:

   typedef std::vector<double> NeatData;
   typedef std::unique_ptr<std::vector<double>> NeatDataUPtr;
   typedef std::shared_ptr<std::vector<double>> NeatDataSPtr;
   typedef std::vector<std::vector<double>> NeatDataCollection;
   typedef std::unique_ptr < std::vector<std::vector<double>>> NeatDataCollectionUPtr;
   typedef std::shared_ptr < std::vector<std::vector<double>>> NeatDataCollectionSPtr;
   typedef std::vector<NeuralNetwork::SPtr> NeuralNetworkCollection;
   typedef NeatData NeatScores;
   typedef std::function<double(NeuralNetwork::SPtr, NeatData)> NeatEvaluationFunction;

   friend class NeuralNetworkPool;

   Neat(const NeatPreferences preferences = NeatPreferences::Std);

   ~Neat();

   const NeatData RunBest(const NeatDataSPtr & input) const;
   
   const NeatData RunID(const int ID, NeatDataSPtr & input) const;

   void ChangeDevice(const NeatDeviceType deviceType);

   double Train(const NeatDataCollectionSPtr& inputCollectionData, const NeatEvaluationFunction evaluationFunction);

   double Train(const NeatEvaluationFunction evaluationFunction);

   void AssignScore(const int ID, const double score);

   void AssignScores(std::vector<int> networks, const NeatScores scores);
   
   void AssignScores(std::map<int, double> nn_score_pair);

   void AssignScores(NeatScores scores);

   void EvaluateScoresAndTrain();

   const NeuralNetwork::SPtr GetNeuralNetworkByID(const int ID) const;

   void StopWhenDone();

   void StopImmediately();

   NeuralNetwork::SPtr BestNetwork() const;

   const bool IsAlive() const;
   
   const NeatPreferences& GetPreferences() const;

private:

   const QString getSalt() const;

   void initializeStorage();

   int addNeuralNetwork();

   std::unique_ptr<NeatCoreStorage> m_storage;

   bool is_alive = false;

   MutationManager mutation_manager;

   SelectionManager selection_manager;

   CrossoverManager crossover_manager;
};
