#include "crossovermanager.h"
#include "numbers.h"

namespace
{
   bool connects_to_same_neurons(Genome::SPtr a, Genome::SPtr b)
   {
      return a->input_neuron->ID == b->input_neuron->ID && a->output_neuron->ID == b->output_neuron->ID;
   }
}

CrossoverManager::CrossoverManager(const NeatPreferences& prefs,
   NeuralNetworkPool& pool)
   : prefs(prefs),
   pool(pool)
{}

void CrossoverManager::recreate_population(std::vector<NeuralNetwork::SPtr> best)
{
   auto first = best[0];
   auto second = best[1];

   auto first_genomes = first->get_genomes();
   auto second_genomes = second->get_genomes();

   auto generation_size = prefs.generation_size - 1;

   for (int i = 0; i < generation_size + 1; i++)
   {
      if (first->ID == i || second->ID == i)
         continue;

      // TODO overwrite method implementation
      NeuralNetwork::SPtr network = pool.overwrite_neural_network(i);

      std::function<bool(Neuron::SPtr&, Neuron::SPtr&)> fn = 
         [](Neuron::SPtr& a, Neuron::SPtr& b) 
            { return a->ID == b->ID && !(a->input_genomes.size() == a->output_genomes.size() == 0); };

      for (auto neuron : combine_expr(first->registered_neurons,
         second->registered_neurons,
         fn))
         network->registered_neurons.push_back(neuron);

      for (auto first_genome : first_genomes)
      {
         for (auto second_genome : second_genomes)
         {
            if (connects_to_same_neurons(first_genome, second_genome))
            {
               auto& selected_genome = take_random(first_genome, second_genome);
               network->registered_genomes.push_back(selected_genome->Clone());
            }
         }
      }

      network->reconnect_genomes();
   }
}
