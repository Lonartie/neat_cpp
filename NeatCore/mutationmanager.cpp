#include "mutationmanager.h"
#include "neatcore.h"
#include "probabilitychecker.h"
#include "numbers.h"

MutationManager::MutationManager(const NeatPreferences& prefs, NeuralNetworkPool& pool)
   :  prefs(prefs),
      pool(pool)
{}

void MutationManager::mutate_all()
{
   for (auto& neural_network : pool.get_networks())
      mutate_network(neural_network);
}

void MutationManager::mutate_network(NeuralNetwork::SPtr neural_network)
{
   // randomly disjoint a neuron
   if (ProbabilityChecker::Is(prefs.disjoint_probability))
      disjoint_random_genome(neural_network);

   // randomly connect neurons
   if (ProbabilityChecker::Is(prefs.genome_probability))
      connect_2_random_neurons(neural_network);

   // randomly reinitialize weight
   if (ProbabilityChecker::Is(prefs.random_weight_probability))
      re_init_random_weight(neural_network);

   // randomly change weight
   for (auto& genome : neural_network->get_genomes())
      if (ProbabilityChecker::Is(prefs.weight_probability))
         change_weight_randomly(genome);

   // randomly add Neuron
   if (ProbabilityChecker::Is(prefs.neuron_probability))
      add_random_neuron(neural_network);
}

void MutationManager::disjoint_random_genome(NeuralNetwork::SPtr neural_network)
{
   auto& genomes = neural_network->registered_genomes;

   int genome_index = random_number<int>(0, genomes.size() - 1);

   if (genome_index >= genomes.size())
      return;
   
   genomes[genome_index]->is_active = !genomes[genome_index]->is_active;
}

void MutationManager::connect_2_random_neurons(NeuralNetwork::SPtr neural_network)
{
   auto neuron_a = pool.get_random_neuron(neural_network);

   if (!neuron_a)
      return;

   auto neuron_b = pool.get_random_neuron(neural_network, neuron_a->ID);

   if (!neuron_b)
      return;

   pool.connect_neurons(neural_network, neuron_a->ID, neuron_b->ID);
}

void MutationManager::re_init_random_weight(NeuralNetwork::SPtr neural_network)
{
   auto& genomes = neural_network->registered_genomes;

   int genome_index = random_number<int>(0, genomes.size());

   if (genome_index >= genomes.size())
      return;

   genomes[genome_index]->weight = random_number<double>(-2, 2);
}

void MutationManager::change_weight_randomly(Genome::SPtr genome)
{
   /*???*/
}

void MutationManager::add_random_neuron(NeuralNetwork::SPtr neural_network)
{
   const auto neuron_a = pool.get_random_neuron(neural_network);

   if (!neuron_a)
      return;

   const auto neuron_b = pool.get_random_neuron(neural_network, neuron_a->ID);

   if (!neuron_b)
      return;

   auto btw = pool.create_neuron(neural_network);

   if (!btw)
      return;

   pool.connect_neurons(neural_network, neuron_a->ID, btw->ID);
   pool.connect_neurons(neural_network, btw->ID, neuron_b->ID);
}