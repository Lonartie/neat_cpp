#pragma once

#include "Export.h"

#include "neuron.h"
#include "genome.h"
#include "neatpreferences.h"

#include <memory>
#include <vector>

class NeuralNetworkPool;

class NEATCORE_EXPORT NeuralNetwork
{
public:

   friend class NeuralNetworkPool;
   friend class MutationManager;
   friend class SelectionManager;
   friend class CrossoverManager;

   MEMORY(NeuralNetwork);

   NeuralNetwork(const NeatPreferences& prefs, NeuralNetworkPool& pool);

   ~NeuralNetwork() = default;

   std::vector<double> run(std::shared_ptr<std::vector<double>> input) const;

   std::vector<std::vector<Neuron::SPtr>>& get_layers();

   std::vector<Genome::SPtr> get_genomes();

   Neuron::SPtr get_neuron_by_ID(int ID) const;

   void reconnect_genomes();
   
   int ID = 0;

private:

   void init_layers();

   std::vector<std::vector<Neuron::SPtr>> layers;

   std::vector<Genome::SPtr> registered_genomes;

   std::vector<Neuron::SPtr> registered_neurons;

   std::map<int /*ID*/, Neuron::SPtr> ID_table;

   const NeatPreferences prefs;

   NeuralNetworkPool& pool;
};