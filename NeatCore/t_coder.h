#pragma once

#include "Export.h"

#include <vector>
#include <string>
#include <unordered_map>
#include <cassert>

template<typename T>
class NEATCORE_EXPORT t_coder
{
public:

   t_coder() = delete;
   t_coder(const t_coder<T>&) = delete;
   ~t_coder() = default;

   t_coder(std::vector<T> list)
   {
      for (int n = 0; n < list.size(); n++)
         m_IDMap.emplace(n, list[n]);

      for (int n = 0; n < list.size(); n++)
         m_ItemMap.emplace(list[n], n);

      for (int n = 0; n < list.size(); n++)
         m_ItemValuesMap.emplace(list[n], n / float(list.size()));
   };

   double GetValue(T item) const
   {
      assert (std::find(m_ItemMap.begin(), m_ItemMap.end(), item) != m_ItemMap.end());
      return m_ItemValuesMap[item];
   };

   int GetID(T item) const
   {
      assert(std::find(m_ItemMap.begin(), m_ItemMap.end(), item) != m_ItemMap.end());
      return m_ItemMap[item];
   };

   T GetItem(int ID) const
   {
      assert(std::find(m_IDMap.begin(), m_IDMap.end(), ID) != m_Itemm_IDMapMap.end());
      return m_IDMap[ID];
   };

   T GetItem(double value) const
   {
      int ID = int(std::round(value * m_IDMap.size()));
      assert(std::find(m_IDMap.begin(), m_IDMap.end(), ID) != m_Itemm_IDMapMap.end());
      return m_IDMap[ID];
   };

   std::vector<double> GetValues() const
   {
      std::vector<double> values;
      for (const auto& item : m_ItemValuesMap)
         values.push_back(item.second);
      return values;
   };

   std::unordered_map<int, T> GetMap() const
   {
      return m_IDMap;
   };

   std::unordered_map<double, T> GetValuesMap() const
   {
      return m_ValuesMap;
   };

private:

   std::unordered_map<int, T> m_IDMap;

   std::unordered_map<T, int> m_ItemMap;

   std::unordered_map<T, double> m_ItemValuesMap;
};