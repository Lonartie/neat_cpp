#pragma once

#include "Export.h"

#include <memory>
#include <vector>
#include <map>
#include <functional>

#include <QtCore/QString>
#include <QtCore/QStringList>

class IActivationFunction;

class NEATCORE_EXPORT ActivationFunctionFactory
{
public:

   static bool RegisterActivationFunction(std::function<std::shared_ptr<IActivationFunction>()> creation_method, QString name);

   static std::shared_ptr<IActivationFunction> CreateInstance(QString name);

   static QStringList GetNames();

   static std::shared_ptr<IActivationFunction> GetRandom();

private:

   static std::map<QString, std::function<std::shared_ptr<IActivationFunction>()>> registered_methods;
};
