#pragma once

#include "Export.h"

enum NEATCORE_EXPORT NeatDeviceType
{
   CPU_SINGLE,
   CPU_MULTI,
   GPU
};