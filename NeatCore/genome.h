#pragma once

#include "Export.h"

#include <memory>
#include <vector>

class Neuron;

class NEATCORE_EXPORT Genome
{
public:

   MEMORY(Genome);

   Genome();
   ~Genome() = default;

   double weight = 0.0;

   bool is_active = true;

   double get_output();

   Genome::SPtr Clone();

   std::shared_ptr<Neuron>
      input_neuron,
      output_neuron;

   int
      input_neuron_ID,
      output_neuron_ID;
};
