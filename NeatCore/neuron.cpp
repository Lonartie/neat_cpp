#include "neuron.h"

#include "genome.h"
#include "numbers.h"

Neuron::Neuron(std::shared_ptr<IActivationFunction> ac_func, int ID, double layer)
   :  ac_func(ac_func),
      ID(ID),
      layer(layer)
{
   init_random();
}

Neuron::Neuron(const Neuron& neuron)
{}

Neuron::~Neuron()
{}

double Neuron::get_output()
{
   if (is_input)
      return value;

   double sum = 0;

   for (const auto& genome : input_genomes)
      if (genome->is_active)
         sum += genome->get_output();

   return ac_func->calculate (sum);
}
