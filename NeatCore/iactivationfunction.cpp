#include "iactivationfunction.h"


NEATCORE_EXPORT DEFINE_ACTIVATION_FUNCTION(SIN,
                           sin(x),
                           cos(x))

NEATCORE_EXPORT DEFINE_ACTIVATION_FUNCTION(COS, 
                           cos(x), 
                           -sin(x))

NEATCORE_EXPORT DEFINE_ACTIVATION_FUNCTION(SIGMOID,
                           1.0 / (1.0 + exp(-x)),
                           exp(-x) / (pow(exp(-x), 2.0) + 2.0 * exp(-x) + 1.0))

NEATCORE_EXPORT DEFINE_ACTIVATION_FUNCTION(LINEAR,
                           x,
                           1)