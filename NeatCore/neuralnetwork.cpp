#include "neuralnetwork.h"
#include "neuralnetworkpool.h"

NeuralNetwork::NeuralNetwork(const NeatPreferences& prefs, NeuralNetworkPool& pool) 
   :  prefs(prefs),
      pool(pool)
{
   init_layers();
}

Neuron::SPtr NeuralNetwork::get_neuron_by_ID(int ID) const
{
   auto iter = ID_table.find(ID);
   if (iter != ID_table.end())
      return ID_table.at(ID);
   return nullptr;
}

void NeuralNetwork::reconnect_genomes()
{
   for (auto genome : registered_genomes)
   {
      auto in_ID = genome->input_neuron_ID;
      auto ou_ID = genome->output_neuron_ID;
      auto in_neuron = get_neuron_by_ID(in_ID);
      auto ou_neuron = get_neuron_by_ID(ou_ID);
      pool.connect_neurons(in_neuron, ou_neuron, genome);
   }
}

std::vector<double> NeuralNetwork::run(std::shared_ptr<std::vector<double>> input) const
{
   // variables
   int lastIndex = layers.size() - 1;
   std::vector<double> output;

   // init the input layer
   for (int index = 0; index < layers[0].size(); index++)
      layers[0][index]->value = input->at(index);

   // get the output from output layer
   for (int index = 0; index < layers[lastIndex].size(); index++)
      output.push_back(layers[lastIndex][index]->get_output());

   // output
   return output;
}

std::vector<std::vector<Neuron::SPtr>>& NeuralNetwork::get_layers()
{
   return layers;
}

std::vector<Genome::SPtr> NeuralNetwork::get_genomes()
{
   return registered_genomes;
}

void NeuralNetwork::init_layers()
{
   int neuronSize = 0;
   int input_size = prefs.input_size;
   int output_size = prefs.output_size;
   std::vector<Neuron::SPtr> input_neurons, output_neurons;

   for (int ID = 0; ID < input_size; ID++)
      input_neurons.push_back(std::make_shared<Neuron>(ActivationFunctionFactory::CreateInstance("LINEAR"), neuronSize++, 0));

   for (int ID = 0; ID < output_size; ID++)
      output_neurons.push_back(std::make_shared<Neuron>(ActivationFunctionFactory::CreateInstance("LINEAR"), neuronSize++, 100));

   layers.push_back(input_neurons);
   layers.push_back(output_neurons);

   for (auto& neuron : input_neurons)
   {
      neuron->is_input = true;
      ID_table.emplace(neuron->ID, neuron);
      registered_neurons.push_back(neuron);
   }

   for (auto& neuron : output_neurons)
   {
      ID_table.emplace(neuron->ID, neuron);
      registered_neurons.push_back(neuron);
   }
}
