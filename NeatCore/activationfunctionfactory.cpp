#include "activationfunctionfactory.h"
#include "numbers.h"

#include "iactivationfunction.h"

std::map<QString, std::function<std::shared_ptr<IActivationFunction>()>> ActivationFunctionFactory::registered_methods;

bool ActivationFunctionFactory::RegisterActivationFunction(std::function<std::shared_ptr<IActivationFunction>()> creation_method, QString name)
{
   registered_methods.emplace(name, creation_method);
   return true;
}

IActivationFunction::SPtr ActivationFunctionFactory::CreateInstance(QString name)
{
   auto iter = registered_methods.find(name);
   if (iter != registered_methods.end())
      return iter->second();
   return nullptr;
}

QStringList ActivationFunctionFactory::GetNames()
{
   QStringList names;
   for (const auto& method : registered_methods)
      names << method.first;
   return names;
}

std::shared_ptr<IActivationFunction> ActivationFunctionFactory::GetRandom()
{
   return registered_methods[GetNames()[random_number<int>(0, registered_methods.size() - 1)]]();
}
