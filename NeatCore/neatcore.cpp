#include "neatcore.h"

#include <cassert>

Neat::Neat(const NeatPreferences preferences)
   :  m_storage(std::make_unique<NeatCoreStorage>(preferences, *this)),
      mutation_manager(m_storage->preferences, m_storage->pool),
      selection_manager(m_storage->pool, m_storage->preferences),
      crossover_manager(m_storage->preferences, m_storage->pool)
{
   is_alive = true;
   initializeStorage();
}

Neat::~Neat()
{
   is_alive = false;
}

const Neat::NeatData Neat::RunBest(const NeatDataSPtr & input) const
{
   auto data = BestNetwork()->run(input);

   return std::move(data);
}

const Neat::NeatData Neat::RunID(const int ID, NeatDataSPtr & input) const
{
   auto data = m_storage->pool.get_networks()[ID]->run(input);
   return data;
}

void Neat::ChangeDevice(const NeatDeviceType deviceType)
{
   m_storage->deviceType = deviceType;
}

double Neat::Train(const NeatDataCollectionSPtr & inputCollectionData, const NeatEvaluationFunction evaluationFunction)
{
   auto best = selection_manager.get_best_population();

   crossover_manager.recreate_population(best);

   mutation_manager.mutate_all();

   for (auto network : m_storage->pool.get_networks())
      for (auto data : (*inputCollectionData))
         AssignScore(network->ID, evaluationFunction(network, data));

   return 0.0;
}

double Neat::Train(const NeatEvaluationFunction evaluationFunction)
{
   auto best = selection_manager.get_best_population();

   crossover_manager.recreate_population(best);

   mutation_manager.mutate_all();

   return 0;
}

void Neat::AssignScore(const int ID, const double score)
{
   selection_manager.set_score(ID, score);
}

void Neat::AssignScores(std::vector<int> networks, const NeatScores scores)
{
   for (int index = 0; index < networks.size(); index++)
      AssignScore(networks[index], scores[index]);
}

void Neat::AssignScores(std::map<int, double> nn_score_pair)
{
   for (auto& kvp : nn_score_pair)
      AssignScore(kvp.first, kvp.second);
}

void Neat::AssignScores(NeatScores scores)
{
   for (int index = 0; index < scores.size(); index++)
      AssignScore(index, scores[index]);
}

void Neat::EvaluateScoresAndTrain()
{
   auto best = selection_manager.get_best_population();

   crossover_manager.recreate_population(best);

   mutation_manager.mutate_all();
}

const NeuralNetwork::SPtr Neat::GetNeuralNetworkByID(const int ID) const
{
   assert(ID < m_storage->pool.get_networks().size() && ID >= 0);
   return m_storage->pool.get_networks()[ID];
}

void Neat::StopWhenDone()
{
}

void Neat::StopImmediately()
{
}

const bool Neat::IsAlive() const
{
   return is_alive;
}

const NeatPreferences& Neat::GetPreferences() const
{
   return m_storage->preferences;
}

NeuralNetwork::SPtr Neat::BestNetwork() const
{
   return selection_manager.get_best();
}

const QString Neat::getSalt() const
{
   return QString::fromWCharArray(
      new wchar_t[6] { wchar_t(2), wchar_t(19), wchar_t(22), wchar_t(200), wchar_t(34), wchar_t(82) }, 6
   );
}

void Neat::initializeStorage()
{
   for (int n = 0; n < m_storage->preferences.generation_size; n++)
      addNeuralNetwork();
   m_storage->deviceType = CPU_MULTI;
}

int Neat::addNeuralNetwork()
{
   auto network = m_storage->pool.create_neural_network();
   return network->ID;
}
