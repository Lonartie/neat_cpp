#pragma once

#include "Export.h"

#include "neatpreferences.h"
#include "neuralnetworkpool.h"
#include "neuralnetwork.h"

#include <map>

class NEATCORE_EXPORT SelectionManager
{
public:

   SelectionManager() = default;

   SelectionManager(NeuralNetworkPool& pool, const NeatPreferences& prefs);

   ~SelectionManager() = default;

   std::vector<NeuralNetwork::SPtr> get_best_population() const;

   NeuralNetwork::SPtr get_best() const;

   void set_score(const NeuralNetwork::SPtr network, double score);

   void set_score(const int ID, double score);

private:

   void add_neural_network_score_definition(NeuralNetwork::SPtr new_nn);

   NeuralNetworkPool& pool;
   
   const NeatPreferences& prefs;

   std::map<NeuralNetwork::SPtr, double> scores;

};
