#include "probabilitychecker.h"

#include <ctime>

bool ProbabilityChecker::is_initialized = false;

bool ProbabilityChecker::Is(double probability)
{
   double num = rand() / (double)RAND_MAX;

   return num <= probability;
}

void ProbabilityChecker::init()
{
   srand(time(0));
}
