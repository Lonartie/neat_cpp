#include "genome.h"

#include "neuron.h"
#include "numbers.h"

Genome::Genome()
   :  weight(random_number<double>(-2, 2))
{}

double Genome::get_output()
{
   return input_neuron->get_output() * weight;
}

Genome::SPtr Genome::Clone()
{
   auto genome = Genome::CreateSPtr();
   genome->input_neuron_ID = input_neuron_ID;
   genome->output_neuron_ID = output_neuron_ID;
   genome->weight = weight;
   genome->is_active = is_active;
   return genome;
}
